package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.module.Configuration;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {
	private static final String SQL_FIND_ALL_USERS="select * from users";
	private static final String SQL_INSERT_USER="insert into users values (default, ?)";
	private static final String SQL_FIND_ALL_TEAMS="select * from teams";
	private static final String SQL_INSERT_TEAM="insert into teams values (default, ?)";
	private static final String SQL_SET_TEAMS_FOR_USER="insert into users_teams values (?, ?)";
	private static final String SQL_GET_USER_TEAMS="select ut.team_id as id, t.name as name from users_teams ut, teams t where user_id = ? and ut.team_id=t.id";
	private static final String SQL_GET_USER="select * from users where login = ?";
	private static final String SQL_GET_TEAM="select * from teams where name = ?";
	private static final String SQL_DELETE_TEAM="delete from teams where name = ?";
	private static final String SQL_UPDATE_TEAM="update teams set name = ? where id = ?";
	private static final String SQL_DELETE_USERS="delete from users where login = ?";

	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
	}
	public static Connection getConnection() throws SQLException {
		Properties prop = new Properties();
		InputStream input = null;
		String connection=new String();
		try {
			input = new FileInputStream("app.properties");
			prop.load(input);
			connection=prop.getProperty("connection.url");
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return DriverManager.getConnection(connection);
	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			con=getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(SQL_FIND_ALL_USERS);
			while (rs.next()) {
				users.add(new User(rs.getInt("id"),rs.getString("login")));
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		finally {
			close(con);
		}
		return users;
	}

	public boolean insertUser(User user) throws DBException {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con=getConnection();
			pstmt = con.prepareStatement(SQL_INSERT_USER,Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, user.getLogin());
			if (pstmt.executeUpdate() > 0) {
				rs = pstmt.getGeneratedKeys();
				if (rs.next()) {
					user.setId(rs.getInt(1));
				}
			}
			return true;
		}catch (SQLException e){
			e.printStackTrace();
			return false;
		}
		finally {
			close(con);
		}
	}

	public boolean deleteUsers(User... users) throws DBException {
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con=getConnection();
			pstmt = con.prepareStatement(SQL_DELETE_USERS);
			for (User user:users){
				pstmt.setString(1, user.getLogin());
				pstmt.executeUpdate();
			}
			return true;
		}catch (SQLException e){
			e.printStackTrace();
			return false;
		}
		finally {
			close(con);
		}
	}

	public User getUser(String login) throws DBException {
		User user = new User();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con=getConnection();
			pstmt = con.prepareStatement(SQL_GET_USER);
			pstmt.setString(1, login);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				user = new User(rs.getInt("id"),rs.getString("login"));
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		finally {
			close(con);
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = new Team();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con=getConnection();
			pstmt = con.prepareStatement(SQL_GET_TEAM);
			pstmt.setString(1, name);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				team = new Team(rs.getInt("id"),rs.getString("name"));
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		finally {
			close(con);
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			con=getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(SQL_FIND_ALL_TEAMS);
			while (rs.next()) {
				teams.add(new Team(rs.getInt("id"),rs.getString("name")));
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		finally {
			close(con);
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con=getConnection();
			pstmt = con.prepareStatement(SQL_INSERT_TEAM,Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, team.getName());
			if (pstmt.executeUpdate() > 0) {
				rs = pstmt.getGeneratedKeys();
				if (rs.next()) {
					team.setId(rs.getInt(1));
				}
			}
			return true;
		}catch (SQLException e){
			e.printStackTrace();
			return false;
		}
		finally {
			close(con);
		}
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con=getConnection();
			con.setAutoCommit(false);
			pstmt = con.prepareStatement(SQL_SET_TEAMS_FOR_USER);
			for(Team team:teams){
				pstmt.setString(1, String.valueOf(user.getId()));
				pstmt.setString(2,String.valueOf(team.getId()));
				pstmt.executeUpdate();
			}
			con.commit();
			return true;
		}catch (SQLException e){
			rollback(con);
			e.printStackTrace();
			throw new DBException();
		}
		finally {
			close(con);
		}
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con=getConnection();
			pstmt = con.prepareStatement(SQL_GET_USER_TEAMS);
			pstmt.setString(1, String.valueOf(user.getId()));
			rs = pstmt.executeQuery();
			while (rs.next()) {
				teams.add(new Team(rs.getInt("id"),rs.getString("name")));
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		finally {
			close(con);
		}
		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con=getConnection();
			pstmt = con.prepareStatement(SQL_DELETE_TEAM);
			pstmt.setString(1, team.getName());
			pstmt.executeUpdate();
			return true;
		}catch (SQLException e){
			e.printStackTrace();
			return false;
		}
		finally {
			close(con);
		}
	}

	public boolean updateTeam(Team team) throws DBException {
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con=getConnection();
			pstmt = con.prepareStatement(SQL_UPDATE_TEAM);
			pstmt.setString(1, team.getName());
			pstmt.setInt(2, team.getId());
			pstmt.executeUpdate();
			return true;
		}catch (SQLException e){
			e.printStackTrace();
			return false;
		}
		finally {
			close(con);
		}
	}

	private void close(Connection con) {
		if (con != null) {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	private void rollback(Connection con) {
		if (con != null) {
			try {
				con.rollback();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
