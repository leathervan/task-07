package com.epam.rd.java.basic.task7.db.entity;

public class Team {

	private int id;

	private String name;

	public Team(int id,String name){
		this.id=id;
		this.name=name;
	}
	public Team(){
		id=0;
		name=null;
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static Team createTeam(String name) {
		return new Team(0,name);
	}
	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
		if (obj.getClass() != this.getClass()) return false;
		final Team other = (Team) obj;
		return this.name.equals(other.name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
